# git 分支使用 (我是开发分支dev)

## 我想测试一下tortoiseGit

## git 分支说明搭建

#### 分支关系

```      
|—— master ——> debug  ——|
|                       | ——> [ 线上出bug的时候 ]
|—— debug  ——> master ——|        ||       ||
|                                ||--> [master中的代码dev中一定要同步]
|—— dev    ——> test   ——|        ||
|                       | ——> [ 开发环境中 ]
|—— test   ——> master ——|     

```

#### master分支【只读分支】
```
master分支为干净分支
与上线代码一模一样
备注：
只能通过 test 或 debug 合并
$ git merge test
$ git merge debug

```
#### debug分支【常规分支】
```
debug分支为线上bug修改分支
与master分支代码一模一样
备注：
当线上存在bug的时候
1.修改debug分支代码进行测试
2.bug修改解决后合并到 master分支

```
#### dev分支 【常规分支】（默认分支）
```
dev分支为开发分支
平时开发中使用
备注：
1.本地开发测试
2.需求开发完毕后合并test测试分支

```
#### test分支【常规分支】
```
test分支为修改dev不过使用
与dev开发完成时的代码一模一样
备注：
1.必须是本地开发完成后合并的代码
2.只供修改测试bug使用
3.最终通过测试后合并到master分支

```
### git分支操作常见命令

1. 创建并切换新分支 $ git checkout -b dev [新建分之后需要通过$ git push origin test 将分支推到远程]

2. 查看当前分支 $ git branch

3. 查看所有分支列表 $ git branch -a  

4. 切换分支 $ git checkout test 

5. 把分支推到远程分支 [如果在当前分支下可以直接使用 $ git push 即可提交到当前分支] $ git push origin test 
 
6. 分支合并[先求换到目的分支下使用命令合并目标分支] $ git merge test

7. 删除本地分支 $ git branch -D dev

8. 删除远程分支 $  git push origin :dev //不加本地分支时进行提交即删除远程分支

9. 将本地分支dev提交到远程分支master  $ git push origin dev:master (省去合并流程：本地合并后还需要push到远程)

10. 放弃某一个文件的修改 $ git reste --hard

11. 回退到某一版本但保存自该版本起的修改 $ git reste 35e2407c115fc0525bcd7f5ae534aca299b884d1

12. 回退到某一版本并且放弃所有的修改 $ git reste --hard 35e2407c115fc0525bcd7f5ae534aca299b884d1

13. 回退到当前版本(放弃所有修改) $ git checkout README.md

14. 查看历史修改记录  $ git log

15. 列出当前目录所有还没有被git管理的文件和被git管理且被修改但还未提交(git commit)的文件 $ git status

16. 只列出所有已经被git管理的且被修改但没提交的文件 $git status -uno